import argparse
import sd_attribute_table

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate Self Description documentation.')
    sub_parser = parser.add_subparsers(title="Output to be generated", dest="output")

    # Setup parser for sd-attribute command
    sd_attribute_parser = sub_parser.add_parser("sd-attributes", help="Creates attribute tables for each SD class in markdown.")
    sd_attribute_parser.add_argument('--srcYaml', type=str, required=True,
                        help='Path to yaml files with attribute definition.')
    sd_attribute_parser.add_argument('--srcConcept', type=str, required=True,
                        help='Path to conceptual model files.')
    sd_attribute_parser.add_argument('--fileName', type=str, required=False,
                        help='Name of markdown file. Default attributes.md.')

    args = parser.parse_args()
    #    , _ = parser.parse_known_args()
    if args.output == "sd-attributes":
        if args.fileName:
            sd_attribute_table.write_markdown(args.srcYaml, args.srcConcept, args.fileName)
        else:
            sd_attribute_table.write_markdown(args.srcYaml, args.srcConcept)
